FROM node

WORKDIR  ./app

COPY . .

RUN npm init -y 
COPY . .
RUN npm i express 
RUN npm i -g nodemon 
EXPOSE 3000

CMD ["nodemon", "index.js"]

